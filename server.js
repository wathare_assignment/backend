const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')

const app = express()

app.use(cors());
app.use(express.json());

// Connect to MongoDB
mongoose.connect('mongodb://localhost:27017/?authMechanism=DEFAULT', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Create a model for your data
const dataSchema = new mongoose.Schema({
  timestamp: {
    type: Date,
    default: Date.now,
  },
  value: Number,
});

const DataModel = mongoose.model('Data', dataSchema);

// Define routes for fetching data
app.get('/api/data', async (req, res) => {
  try {
    const data = await DataModel.find().sort('-timestamp').limit(100); // Fetch the last 100 data entries
    res.json(data);
  } catch (error) {
    res.status(500).json({ message: 'Server error' });
  }
});


app.listen(4000, () => {
    console.log('Server started on port no 4000')
})